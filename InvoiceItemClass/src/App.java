public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        InvoiceItem invoiceItem1 = new InvoiceItem("HD002", "Áo Croptop", 4,20000);
        InvoiceItem invoiceItem2 = new InvoiceItem("HD002", "Quần Jeans", 10, 10000);
        //thông tin hóa đơn
        System.out.println("thông tin hóa đơn " + invoiceItem1.toString());
        System.out.println("thông tin hóa đơn " + invoiceItem2.toString());
        //số tiền phải thanh toán
        System.out.println("số tiền phải thanh toán là " + invoiceItem1.getTotal());
        System.out.println("số tiền phải thanh toán là " + invoiceItem2.getTotal());
    }
}
